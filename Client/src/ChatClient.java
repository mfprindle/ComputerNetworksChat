import java.net.*;
import java.io.*;

public class ChatClient {

    private Socket socket;
    private ServerReaderThread sReader;

    public static void main(String []args) throws IOException {
        ChatClient client = new ChatClient();
        client.start();
    }

    private void start() throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //used for getting hostname and port
        System.out.println("Please enter a host name or IP address: ");
        String hostName = br.readLine();
        System.out.println("Please enter a port number: ");
        int portNumber = Integer.parseInt(br.readLine());

        try {
            socket = new Socket(hostName, portNumber);
            sReader = new ServerReaderThread(socket);
            Thread fromSever = new Thread(sReader);
            fromSever.start();

            getInput();


        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        }  catch (ConnectException e) {
            System.out.println("Couldn't connect to the server on port " + portNumber + ".");
        } catch (IOException e) {
            System.out.println("Couldn't get I/O for the connection to " +
                    hostName + ".");
            System.exit(1);
        }
    }

    class ServerReaderThread implements Runnable {
        private final Socket socket;
        private volatile boolean execute;

        ServerReaderThread(Socket socket) {
            this.socket = socket;
        }


        @Override
        public void run() {
            this.execute = true;
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                while (this.execute) {
                    String message = in.readLine();

                    // Catch an improperly closed socket
                    if (message == null) {
                        stopExecuting();
                        break;
                    }
                    System.out.println(message);
                }
            } catch (IOException e) {
                stopExecuting();
            }
        }

        // Handles all shutdown cases
        void stopExecuting() {
            this.execute = false;
            try {
                socket.close();
            } catch (IOException e) {
                System.err.println("The socket was already closed somehow.");
            }
            System.out.println("The server has closed the connection.");
            System.out.println("Press enter to close the client.");
        }

    }




    private void getInput() {
        try {
            PrintWriter out =
                    new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in =
                    new BufferedReader(
                            new InputStreamReader(System.in));
            while (sReader.execute) {
                String clientOutput;
                clientOutput = in.readLine();
                out.println(clientOutput);
            }
        } catch (IOException e) {
            System.err.println("Somehow you tried writing to a closed socket. This shouldn't happen");
        }
    }

}
