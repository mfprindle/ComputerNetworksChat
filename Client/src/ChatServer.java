import java.net.*;
import java.io.*;
import java.util.*;

public class ChatServer {

    // Arbitrarily determined maximum number of users
    private static final int MAX_USERS = 10;

    // The list of running threads
    private ArrayList<ClientConnection> clients;
    // A list of clients and their associated usernames
    private Hashtable<Runnable, String> usernames;

    // Used for server shutdown
    private boolean listening = true;

    private ChatServer() {

        clients = new ArrayList<>();
        usernames = new Hashtable<>();

    }

    public static void main(String args[]) throws IOException {
        ChatServer server = new ChatServer();
        server.start();
    }

    private void start() throws IOException {


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int portNumber;
        while (true) {
            System.out.println("Please enter a valid port number: ");
            try {
                portNumber = Integer.parseInt(br.readLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid port number.");
            }
        }

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            System.out.println("Server is active and listening for clients.");
            System.out.println("Enter \"shutdown\" to close the server.");

            Thread listenShutdown = new Thread(new ShutdownReader(br));
            listenShutdown.start();

            while (listening) {
                ClientConnection newClient = new ClientConnection(serverSocket.accept());
                clients.add(newClient);
                Thread newClientThread = new Thread(newClient);
                newClientThread.start();
            }
        } catch (IOException e) {
            System.err.println("Couldn't listen on port " + portNumber);
            System.exit(-1);
        }
    }


    // A class used to enable shutdown of the server
    class ShutdownReader implements Runnable {
        private BufferedReader br;
        private boolean execute = true;

        ShutdownReader(BufferedReader br) {
            this.br = br;
        }

        @Override
        public void run() {
            while (execute) {
                try {
                    String input = br.readLine();
                    if (input.equalsIgnoreCase("shutdown")) {
                        System.out.println("Server has closed down.");
                        System.exit(0);
                    }
                } catch (IOException e) {
                    System.err.println("I/O error reading from command line somehow.");
                }
            }
        }
    }

    // A thread that handles all the behavior for a single client connection
    class ClientConnection implements Runnable {
        private Socket socket;
        private PrintWriter out;
        private BufferedReader in;
        private volatile boolean execute;

        ClientConnection(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            this.execute = true;
            try {
                out =
                        new PrintWriter(socket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
            } catch (IOException e) {
                System.err.println("Something went wrong with I/O");
            }
            try {

                // Disallow users over a maximum capacity
                if (atCapacity()) {
                    out.println("Server is at capacity.");
                    stopExecuting();
                }
                registerUser(in, out);
                while (this.execute) {
                    String clientInput = in.readLine();
                    if (clientInput == null) {
                        stopExecuting();
                        break;
                    } else {
                        processMessage(clientInput);
                    }
                }
            }catch (IOException e) {
                stopExecuting();
            }
        }

        /**
         * Processes a message received from the client and executes
         * the appropriate response.
         */
        private void processMessage(String clientInput) {
            String inputUppercase = clientInput.toUpperCase();
            switch (inputUppercase) {
                case "/HELP" : //user requests a list of commands
                    out.println("Type \"\\users\" or press the users button for a list of users.");
                    out.println("Type \"\\disconnect\" or press the disconnect button to disconnect.");
                    out.println("Type \"@user\", followed by your message body, to PM the specified user.");
                    break;
                case "/DISCONNECT" : //user wishes to disconnect
                    out.println("Goodbye!");
                    stopExecuting();
                    break;
                case "/USERS" : //user requests list of connected users
                    for (Map.Entry<Runnable, String> entry : usernames.entrySet()) {
                        String value = entry.getValue();
                        out.println(value);
                    }
                    break;
                default:
                    broadcast(clientInput);
                    break;
            }
        }


        // Sends a message to the appropriate clients
        private void broadcast(String clientInput) {

            // Don't broadcast nothing!
            if (clientInput.trim().isEmpty()) {
                return;
            }

            // A private message
            char startsWith = clientInput.charAt(0);
            if (startsWith == '/') {
                return; // Essentially a placeholder for any future commands
            }
            else if (startsWith == '@') {
                String toBroadcast = clientInput.substring(1);
                if (!toBroadcast.trim().isEmpty() && toBroadcast.contains(" ")) {
                    String components[] = toBroadcast.split(" ", 2);
                    String destination = components[0];
                    String body = components[1];
                    if (!usernames.contains(destination)) {
                        out.println("Sorry, couldn't find user \"" + destination + "\".");
                    } else {

                        // Don't PM yourself
                        Hashtable temp = (Hashtable) usernames.clone();
                        temp.remove(this);
                        if (temp.contains(destination)) {
                            for (ClientConnection client : clients) {
                                if (usernames.get(client).equals(destination)) {
                                    client.out.println("PM from " + destination + ": " + body);
                                    break;
                                }
                            }
                            out.println("To " + destination + ": " + body);
                        }
                    }
                }
            }

            // A regular message
            else {
                String signature = usernames.get(this);
                for (ClientConnection client : clients) {
                    client.out.println(signature + ": " + clientInput);
                }
            }
        }

        /**
         * Kills the client connection thread in the case of any kind
         * of client disconnect. Specifies which client has disconnected
         * if the client has already registered.
         */
        void stopExecuting() {
            this.execute = false;
            String username = usernames.get(this);
            if (username == null || username.equals("")) {
                System.out.println("An unregistered client has disconnected");
            }
            else {
                System.out.println(username + " has left the chat room.");

                // Notify other users of client disconnection
                for (ClientConnection client : clients) {
                    if (!(usernames.get(client) == null) && !usernames.get(client).equals(username)) {
                        client.out.println(username + " has left the chat room. Goodbye, " + username + "!");
                    }
                }
            }
            try {
                socket.close();
                usernames.remove(this);
                clients.remove(this);
            } catch (IOException e) {
                System.err.println("Socket closing failed somehow");
            }
        }

        /**
         * Using the input and output stream associated with the given
         * client connection thread, acquires a username for the new
         * client, and welcomes them to the chat room.
         */
        void registerUser(BufferedReader in, PrintWriter out) throws IOException{
            String username = "";
            boolean needsID = true;
            while (needsID) {
                out.println("Please enter a username: ");
                username = in.readLine();
                if (username == null  || (!username.matches("[a-zA-Z0-9]*")) || (username.length() > 15)) {
                    out.println("Username must be 15 characters of less \n and contain only letters and numbers.");
                } else if (usernames.contains(username)) {
                    out.println("Username " + username + " is already taken.");
                } else {
                    System.out.println(username + " has joined the chat room.");

                    // Notify other users of new client
                    for (ClientConnection client : clients) {
                        if (!(usernames.get(client) == null) && !usernames.get(client).equals(username)) {
                            client.out.println(username + " has joined the chat room. Welcome, " + username + "!");
                        }
                    }

                    // Welcome user to chat room
                    out.println("Welcome to the chat room, " + username + "!");
                    needsID = false;
                }
            }
            usernames.put(this, username);
        }


        /**
         * Checks if the server already has the arbitrarily determined maximum number of users
         * connected. Users > rather than >= because a thread is added to the list before it is
         * registered.
         */
        boolean atCapacity() {
            return (clients.size() > MAX_USERS);
        }
    }
}
